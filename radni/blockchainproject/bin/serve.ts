import {Sunshine} from "sunshine-dao/lib/Sunshine";
import {Juice} from "@juice/juice/Juice";
import {BlockchainApp} from "../src/BlockchainApp";
import {Wallet} from "../src/models/Wallet";

const Web3 = require('web3');

const db = {
    username: '',
    password: '',
    host: 'localhost',
    db: 'blockchain-app',
};

Sunshine.connect(db.host, db.username, db.password, db.db ).then(success => {
    new Juice();
    Juice.start(new BlockchainApp()).then(()=> {
        console.log('start')
    })
})



