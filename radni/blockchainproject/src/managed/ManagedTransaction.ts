import {Managed} from "@juice/juice/core/managed/Managed";
import {Transaction} from "../models/Transaction";

export class ManagedTransaction extends Managed<Transaction>{

    constructor(transaction?: Transaction) {
        super(Transaction);
        this.model = transaction ? transaction : new Transaction();
    }

}
