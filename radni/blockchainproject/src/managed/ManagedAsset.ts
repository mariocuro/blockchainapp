import {Managed} from "@juice/juice/core/managed/Managed";
import {Asset} from "../models/Asset";

export class ManagedAsset extends Managed<Asset>{

    constructor(asset?: Asset) {
        super(Asset);
        this.model = asset ? asset : new Asset();
    }

}
