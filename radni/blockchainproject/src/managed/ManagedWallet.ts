import {Wallet} from "../models/Wallet";
import {Managed} from "@juice/juice/core/managed/Managed";

export class ManagedWallet extends Managed<Wallet>{

    constructor(wallet?: Wallet) {
        super(Wallet);
        this.model = wallet ? wallet : new Wallet();
    }

    // TODO: Do this
    public async getBalance(): Promise<Array<{ asset: string, quantity: number }>> {
        return 0;
    }

}
