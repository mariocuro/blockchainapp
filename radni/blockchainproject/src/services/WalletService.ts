import {IService} from "@juice/juice/core/service/IService";
import {ServiceConfiguration} from "@juice/juice/core/decorators/ServiceConfiguration";
import {Wallet} from "../models/Wallet";
import {Asset} from "../models/Asset";
const Web3 = require('web3');
import EthCrypto from "eth-crypto";
import {ManagedWallet} from "../managed/ManagedWallet";
import {Transaction} from "../models/Transaction";

@ServiceConfiguration({
    key: 'WalletService',
    name: 'WalletService',
    options: {}
})

export class WalletService implements IService{
    options: any;

    async onConfigure(): Promise<boolean> {
        return true;
    }

    onReady(): any {
    }

    async onShutdown(): Promise<boolean> {
        return true;
    }

    async onStartup(): Promise<boolean> {
        await this.generateWallets();
        await this.generateAssets();
        await this.transfer();
        return true;
    }

    async onUpdate(options: any): Promise<boolean> {
        return true;
    }

    public async generateWallets(){
        const web3 = new Web3('http://localhost:3000');
        const walletCount = await Wallet.find<Wallet>({}).count();

        if (walletCount != 0)
            return;

        for (let i=0; i<1001; i++){
            const wallet = new Wallet();
            const rootAccount = web3.eth.accounts.create();
            wallet.address = rootAccount.address;
            wallet.privateKey = rootAccount.privateKey;
            if (i == 0){
                wallet.root = true;
                const rootAsset = await this.generateRootAsset(wallet.address);
                wallet.assets.push(rootAsset);
            }
            else {
                wallet.root = false;
                wallet.assets = [];
            }
            wallet.created = new Date();
            await wallet.save();
        }



        return;
    }

    public async generateRootAsset(walletAddress){
        const web3 = new Web3('http://localhost:3000');
        const assetCount = await Asset.find<Asset>({}).count();

        if(assetCount == 0){
            const asset = new Asset();
            const rootAsset = web3.eth.accounts.create();
            asset.name = 'juice-root-1';
            asset.address = rootAsset.address;
            asset.privateKey = rootAsset.privateKey;
            asset.childAssets = [];
            asset.root = true;
            asset.publisherId = walletAddress;
            await asset.save();
            return asset;
        }

    }

    public async generateAssets(){
        const assetCount = await Asset.find<Asset>({root: false}).count();
        const rootAsset = await Asset.findOne<Asset>({root: true});

        if (assetCount == 0){
            for (let i=0; i<1000; i++){
                const asset = new Asset();
                asset.name = 'juice' + (i+1);
                asset.quantity = 1;
                asset.root = false;
                await asset.save();
                await Asset.updateOne({root: true}, {$push: {"childAssets": asset.name}});
            }
        }
    }

    public sign(sender, receiver, asset){

        const body = {
            sender: sender.address,
            receiver: receiver.address,
            payload: {name: asset}
        }

        const messageHash = EthCrypto.hash.keccak256(JSON.stringify(body));

        const signature = EthCrypto.sign(
            sender.privateKey,
            messageHash
        );

        (<any>body).signature = signature;

        return Buffer.from(JSON.stringify(body)).toString('base64');
    }

    public async verify(data){
        //let data = {"sender":"0xCfBCA7F628CE11D9052D69D68c9fBc72E03e50e1","receiver":"0x10Bf3A3D2B1c5b63DCc8EdEeFcaAf0Df0f67eA08",
          //  "payload":{"name":"juice1","quantity":1},"signature":"0x49291e8f2c740fee43aa2d1514126da2256b8b39394041cbb953e91a2a1a280626de5a12c7bcd7e2a56037d2bf574fe6b792f858f6d9a91d88f1d9a9351837aa1c"}
        const signature  = data.signature;
        delete data.signature;
        const hash = EthCrypto.hash.keccak256(JSON.stringify(data));
        const recoveredAddress = EthCrypto.recover(signature, hash);

        if(data.sender === recoveredAddress){
            return true;
        } else {
            return false;
        }
    }

    public async transfer() {
        const sender = await Asset.findOne<Asset>({root: true});
        const receivers = await Wallet.find<Wallet>({root: false}).toArray();

        for (let i = 0; i < sender.childAssets.length; i++) {
            let data: any = this.sign(sender, receivers[i], sender.childAssets[i]);
            data = JSON.parse(Buffer.from(data, 'base64').toString());
            // if (i == 2) {
            //     data.sender = 'KJASHFG54WEG54W5243W3E4G3WE4G';
            // }
            const verify = await this.verify(data);
            if (verify == true) {
                await this.generateTransaction(sender.address, receivers[i].address, sender.childAssets[i]);
                await Wallet.updateOne({address: receivers[i].address}, {$push: {"assets": sender.childAssets[i]}});
                await Asset.updateOne({root: true}, {$pull: {"childAssets": sender.childAssets[i]}})
            }
        }
    }

    public async generateTransaction(senderAddress, receiverAddress, asset) {
        const _transaction = new Transaction();
        _transaction.sender = senderAddress;
        _transaction.receiver = receiverAddress;
        _transaction.asset = asset;

        await _transaction.save();
    }
}
