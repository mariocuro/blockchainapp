import {Collection, Model} from 'sunshine-dao/lib/Model';

@Collection('transactions')
export class Transaction extends Model {

    receiver: string;
    sender: string;
    asset: string;
    quantity: number;
    signature: string;
}
