import {Collection, Model} from 'sunshine-dao/lib/Model';

@Collection('assets')
export class Asset extends Model {

    created: Date;
    name: string;
    quantity: number;
    root: boolean;
    childAssets?: [];
    address?: string;
    privateKey?: string;
    publisherId: string;
}
