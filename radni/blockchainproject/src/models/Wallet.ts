import {Collection, Model} from 'sunshine-dao/lib/Model';

@Collection('wallets')
export class Wallet extends Model {

    created: Date;
    address: string;
    privateKey: string;
    assets = [];
    root: boolean;
}
