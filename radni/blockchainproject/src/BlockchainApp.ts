import {IJuiceApplication} from "@juice/juice/core/IJuiceApplication";
import {Juice} from "@juice/juice/Juice";
import {ColoredConsoleLogger} from "@juice/juice/services/logging/ColoredConsoleLogger";
import {NetworkingService} from "@juice/networking/NetworkingService";
import {ApplicationConfiguration} from "@juice/juice/core/decorators/ApplicationConfiguration";
import {WalletService} from "./services/WalletService";

@ApplicationConfiguration({
    key: "juice:blockchain:app",
    options: {
        logger: "logger:colored-console"
    }
})

export class BlockchainApp implements IJuiceApplication {
    async configure(): Promise<boolean> {
        //register services

        Juice.register(ColoredConsoleLogger);
        await Juice.deployService(NetworkingService);
        await Juice.deployService(WalletService);
        // await Juice.deployService(MealService);
        // Juice.register(MealProvider);

        console.log('configure')
        return true;
    }

    async prepare(): Promise<boolean> {
        console.log('prepare')
        return true;
    }

    async ready(): Promise<any> {
        console.log('ready')
        let networking = Juice.service<NetworkingService>("networking");
        // await networking.deployRoute("/meals", new MealRouteHandler());
        return true;
    }
}
